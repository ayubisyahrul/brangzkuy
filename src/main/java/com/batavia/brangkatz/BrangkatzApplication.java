package com.batavia.brangkatz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan

public class BrangkatzApplication {
	public static void main(String[] args) {
		SpringApplication.run(BrangkatzApplication.class, args);
	}
}
