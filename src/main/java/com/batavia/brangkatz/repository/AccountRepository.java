package com.batavia.brangkatz.repository;

import com.batavia.brangkatz.model.Account;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Long> {

    List<Account> findAllByOrderByAccountId(Pageable pageable);
}
