package com.batavia.brangkatz.controller;

import com.batavia.brangkatz.model.Account;
import com.batavia.brangkatz.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/account")
public class AccountController {
    @Autowired
    AccountRepository accountRepository;

    @GetMapping("/getAccount")
    public List<Account> findAccount(Pageable page){
        return accountRepository.findAllByOrderByAccountId(page);
    }


}
