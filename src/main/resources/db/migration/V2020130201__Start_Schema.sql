CREATE TABLE account (
    account_id SERIAL PRIMARY KEY,
    create_at timestamp without time zone,
    deleted character(1) NOT NULL,
    email character varying(255),
    fname character varying(255),
    is_active boolean NOT NULL,
    lname character varying(255),
    password character varying(255),
    phone character varying(255),
    update_at timestamp without time zone,
    username character varying(255),
    create_by bigint,
    update_by bigint
);